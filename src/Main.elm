import Browser exposing (..)
import Html exposing (..)
import Html.Events exposing (..)
import Html.Attributes exposing (..)

main : Program () Model msg
main =
  Browser.element
  { init = always init
  , update = update
  , subscriptions = always Sub.none
  , view = view
  }

type alias Post = String

post1 = "My first post"
post2 = "Another exciting post"
posts = [post1, post2]

type alias Model =
  { title : String
  , posts : List Post
  }

init : (Model, Cmd msg)
init =
  ( Model "Example Elm site using GitLab Pages" posts
  , Cmd.none )

update : msg -> Model -> (Model, Cmd msg)
update msg model = (model, Cmd.none)

view : Model -> Html msg
view model =
  div [ class "container" ]
    [ h1 [ class "text-center" ] [ text model.title ]
    , h2 [ class "text-center" ] [ text "Posts" ]
    , div [] (List.map viewPost model.posts)
    ]

viewPost : Post -> Html msg
viewPost post = p [] [text post]
